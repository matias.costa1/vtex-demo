import { TextInput, TextBody, Button } from "@fravega-it/bumeran-ds-fvg";
import cookieCutter from 'cookie-cutter'
import Image from "next/image";
import styled from "styled-components";
import logo from "@images/bumeran-iso.svg";
import { useState } from "react";

const Container = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  height: 100vh;
  gap: 8px;
  text-align: center;

  img {
    margin: 0 auto 10px;
  }
`;

const Vtex = (): JSX.Element => {
  const [cookie, setCookie] = useState("")
  const [endpoint, setEndpoint] = useState("")

  const demoCookie = async () => {
    cookieCutter.set('VtexCookieUserClient_fravega', cookie)
    await fetch(endpoint, {
      method: "POST",
      credentials: "same-origin"
    })
  }

  return (
    <Container>
      <Image priority src={logo} alt="Búmeran logo" />
      <TextBody size="l">Frávega Vtex Cookie Demo</TextBody>
      <TextInput id="destinationEndpoint" label="Destination Endpoint" value={endpoint} onChange={value => setEndpoint(value)} />
      <TextInput id="vtexCookie" label="Vtex Cookie" value={cookie} onChange={value => setCookie(value)} />
      <Button label="Demo" onClick={() => demoCookie()} />
    </Container>
  );
};

export default Vtex;
